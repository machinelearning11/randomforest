package com.company;
import java.util.ArrayList;
import java.util.Arrays;

class Id3TreeNode implements Cloneable{
	int id_in_feature;
	int id_for_attribute_list;
	int no_of_features;
	ArrayList<String[]> data;
	String req_param;
	String[] Group;
	private Id3TreeNode prev;
	ArrayList<Id3TreeNode> next = new ArrayList<>();
	boolean isLeaf = false;
	boolean[] useStatusOfAttribute;
	String leafValue = null;
	Id3TreeNode(ArrayList<String[]> data, int id, String req_param, String[] strGrp,
				Id3TreeNode prev, int count, int id_to_status)
	{
		this.no_of_features = count;
		this.prev = prev;
		this.data = data;
		this.id_for_attribute_list = id;
		this.req_param = req_param;
		this.Group = strGrp;
		if (this.prev != null && id != -1 && id_to_status != -1)
		{
			this.useStatusOfAttribute = this.prev.useStatusOfAttribute.clone();
			this.useStatusOfAttribute[id_to_status] = true;
		}
		else {
			this.useStatusOfAttribute = new boolean[count];
			Arrays.fill(this.useStatusOfAttribute, false);
		}
	}

	/**
	 *
	 * @param root The root node
	 * @return The depth of the tree
	 */
	static int treeDepth(Id3TreeNode root)
	{
		if (root == null) {
			return 0;
		}
		int depth = 0;
		for (Id3TreeNode node: root.next)
		{
			depth = Math.max(depth, treeDepth(node));
		}
		return depth + 1;
	}
}