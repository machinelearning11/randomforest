package com.company;

import java.io.*;
import java.util.*;

class RandomForest {
	private File input_file;
	private File test_file;
	private ArrayList<String[]> trainingSet;
	private RFData UsefulSet = new RFData();
	private ArrayList<FinalResult> list_of_trees;

	RandomForest(File input_file, File test_file) throws IOException {
		this.input_file = input_file;
		this.test_file = test_file;
		this.trainingSet = new ArrayList<>();
		this.list_of_trees = new ArrayList<>();
	}

	private void ParseFile() throws IOException {
		String line;
		String[] each_line;
		Scanner sc = new Scanner(input_file);
		while (sc.hasNextLine()) {
			line = sc.nextLine();
			each_line = line.split(",");
			trainingSet.add(each_line);
		}
	}

	/**
	 * Random Forest Implementation Starts Here
	 */
	void startImplementation(int ch) throws IOException, NoSuchFieldException, IllegalAccessException {
		int i, count_of_attrs = UsefulSet.attributes_arr.length, k, j;
		ParseFile();
		int numOfIterations, first, last, data_sz = this.trainingSet.size();
		numOfIterations = 100;
		int[] features;
		for (i = 0; i < numOfIterations; i++) {
			Random rand = new Random();
			if (ch == 1) {
				k = rand.nextInt(count_of_attrs) % count_of_attrs + 1;
				features = new int[k];
				boolean[] feature_use_status = new boolean[14];
				Arrays.fill(feature_use_status, false);
				for (j = 0; j < k; j++) {
					int attrs_index;
					do {
						attrs_index = rand.nextInt(count_of_attrs) % count_of_attrs;
					} while (feature_use_status[attrs_index]);
					features[j] = attrs_index;
					feature_use_status[attrs_index] = true;
				}
				Id3TreeNode root;
				MaxGainStruct gainValue;
				gainValue = UsefulSet.maxInformationGain(this.trainingSet, features, null);
				root = new Id3TreeNode(this.trainingSet, features[gainValue.position],
						UsefulSet.attributes_arr[features[gainValue.position]],
						UsefulSet.feature_list.get(features[gainValue.position]),
						null, k, gainValue.position);
				makeTree(root, features);
				double accuracy = computeAccuracy(this.test_file, root);
				this.list_of_trees.add(new FinalResult(root, accuracy, features));
				System.out.println(accuracy + " " + features.length);
			}
			else {
				features = new int[14];
				first = rand.nextInt(data_sz) % data_sz;
				last = rand.nextInt(data_sz) % data_sz;
				if (first > last)
				{
					int t = first;
					first = last;
					last = t;
				}
				ArrayList<String []> subset_of_TD = new ArrayList<>(this.trainingSet.subList(first, last));
				System.out.println("Subset in range (" + first + ", " + last + ") and size is " +
						subset_of_TD.size());
				Id3TreeNode root;
				MaxGainStruct gainValue;
				gainValue = UsefulSet.maxInformationGain(this.trainingSet, features, null);
				root = new Id3TreeNode(subset_of_TD, features[gainValue.position],
						UsefulSet.attributes_arr[features[gainValue.position]],
						UsefulSet.feature_list.get(features[gainValue.position]),
						null, 14, gainValue.position);
				makeTree(root, features);
				double accuracy = computeAccuracy(this.test_file, root);
				this.list_of_trees.add(new FinalResult(root, accuracy, features));
				System.out.println(accuracy + " " + (last - first + 1));
			}
		}
		// get the tree with maximum accuracy
		Id3TreeNode best_node;
		double max_acc = 0;
		int no_of_features = 0;
		for (i = 0; i < numOfIterations; i++) {
			FinalResult each = this.list_of_trees.get(i);
			if (each.accuracy > max_acc) {
				max_acc = each.accuracy;
				best_node = each.root;
				no_of_features = each.list_of_features.length;
			}
		}
		System.out.println("The maximum accuracy obtained is " + 100 * max_acc + "% with " +
				no_of_features + " feature(s)");
	}

	/**
	 *
	 * @param node The parent node to calculate its children
	 * @param features The list of features using which we are maing the tree
	 * @throws IllegalAccessException For handling access of non-exisitent attributes
	 * @throws IOException For handling all IO Exception
	 */
	private void makeTree(Id3TreeNode node, int[] features) throws IllegalAccessException, IOException {
		if (node.isLeaf) {
			return;
		}
		node.useStatusOfAttribute[node.id_in_feature] = true;
		for (String attrVal : node.Group) {
			Id3TreeNode new_node;
			ArrayList<String[]> new_data = getDataByParameter(node.data, node.id_for_attribute_list, attrVal);
			RFData getInfoGain = new RFData();
			if (new_data.size() == 0) {
				continue;
			}
			try {
				MaxGainStruct struct = getInfoGain.maxInformationGain(new_data, features, node.useStatusOfAttribute);
				if (struct.position != -1) {
					new_node = new Id3TreeNode(new_data, features[struct.position], attrVal,
							getInfoGain.feature_list.get(features[struct.position]), node, features.length, struct.position);
				} else {
					new_node = new Id3TreeNode(new_data, struct.position, attrVal,
							null, node, features.length, -1);
					new_node.isLeaf = true;
					new_node.leafValue = new_data.get(0)[14];
				}
				node.next.add(new_node);
				makeTree(new_node, features);
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			}
		}
	}

	private ArrayList<String[]> getDataByParameter(ArrayList<String[]> data, int param, String value)
			throws IllegalAccessException {
		ArrayList<String[]> new_data = new ArrayList<>();
		for (String[] ad : data) {
			if (param == 0 || param == 2 || param == 4 || param == 10 || param == 11 || param == 12) {
				int val = Integer.parseInt(ad[param]);
				int low = Integer.parseInt(value.split(",")[0]);
				int high = Integer.parseInt(value.split(",")[1]);
				if (low <= val && val < high) {
					new_data.add(ad);
				}
			} else {
				if (value.equals(ad[param])) {
					new_data.add(ad);
				}
			}
		}
		return new_data;
	}

	/**
	 *
	 * @param f File name for getting test data
	 * @param root The root node for traversing te tree
	 * @return Acccuracy of the tree with the training data
	 */
	private double computeAccuracy(File f, Id3TreeNode root) {
		double accuracy, total = 0, correct = 0;
		String line;
		String each_line[];
		boolean result;
		try {
			Scanner sc = new Scanner(f);
			while (sc.hasNextLine()) {
				line = sc.nextLine();
				each_line = line.split(",");
				result = traverseTree(root, each_line);
				if (result) {
					correct++;
				}
				total++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		accuracy = correct / total;
		return accuracy;
	}

	/**
	 *
	 * @param node Node Root for traversing the treee
	 * @param line Get the value to check if tree leaf gives the right value after traversing
	 * @return TRUE/FALSE
	 */
	private boolean traverseTree(Id3TreeNode node, String[] line) {
		boolean res = false;
		int id = node.id_for_attribute_list;
		if (id == -1) {
			String answer = line[14];
			if (node.leafValue.equals(answer)) {
				res = true;
			}
		} else {
			String test_value = line[id];
			if (id == 0 || id == 2 || id == 4 || id == 10 || id == 11 || id == 12) {
				int val = Integer.parseInt(test_value);
				int size = node.next.size();
				for (int i = 0; i < size; i++) {
					int lower = Integer.parseInt(node.next.get(i).req_param.split(",")[0]);
					int higher = Integer.parseInt(node.next.get(i).req_param.split(",")[1]);
					if (val >= lower && val < higher) {
						res = traverseTree(node.next.get(i), line);
						break;
					}
				}
			} else {
				int size = node.next.size();
				for (int i = 0; i < size; i++) {
					if (test_value.equals(node.next.get(i).req_param)) {
						res = traverseTree(node.next.get(i), line);
						break;
					}
				}
			}
		}
		return res;
	}
}

