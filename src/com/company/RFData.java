package com.company;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

class RFData {
	String[] attributes_arr = {"age", "workclass", "fnlwgt", "education", "education_num", "marital_status", "occupation"
			, "relationship", "race", "sex", "capital_gain", "capital_loss", "hours_per_week", "native_country"};
	String[] strWorkClass = {"Private", "Self-emp-not-inc", "Self-emp-inc", "Federal-gov", "Local-gov", "State-gov", "Without-pay"
			, "Never-worked"};
	String[] strEducation = {"Bachelors", "Some-college", "11th", "HS-grad", "Prof-school", "Assoc-acdm","Assoc-voc",
			"9th", "7th-8th", "12th", "Masters", "1st-4th", "10th", "Doctorate", "5th-6th", "Preschool"};
	String[] strMartial = {"Married-civ-spouse", "Divorced", "Never-married", "Separated", "Widowed",
			"Married-spouse-absent", "Married-AF-spouse"};
	String[] strOccupation = {"Tech-support", "Craft-repair", "Other-service", "Sales", "Exec-managerial", "Prof-specialty",
			"Handlers-cleaners", "Machine-op-inspct", "Adm-clerical", "Farming-fishing", "Transport-moving", "Priv-house-serv",
			"Protective-serv", "Armed-Forces"};
	String[] strRelationship = {"Wife", "Own-child", "Husband", "Not-in-family", "Other-relative", "Unmarried"};

	String[] strRace = {"White", "Asian-Pac-Islander", "Amer-Indian-Eskimo", "Other", "Black"};
	String[] strSex = {"Female", "Male"};
	String[] strCountry = {"United-States", "Cambodia", "England", "Puerto-Rico", "Canada", "Germany", "Outlying-US(Guam-USVI-etc)", "India",
			"Japan", "Greece", "South", "China", "Cuba", "Iran", "Honduras", "Philippines", "Italy", "Poland", "Jamaica", "Vietnam", "Mexico", "Portugal",
			"Ireland", "France", "Dominican-Republic", "Laos", "Ecuador", "Taiwan", "Haiti", "Columbia", "Hungary", "Guatemala", "Nicaragua", "Scotland",
			"Thailand", "Yugoslavia", "El-Salvador", "Trinadad&Tobago", "Peru", "Hong", "Holand-Netherlands"};
	public String[] strAge = {"17,30", "30,45","45,60", "60,91"};
	public String[] strfnlwgt = {"12285,60000", "60000,1484705"};
	public String[] strEduNum = {"1,8", "8,17"};
	public String[] strCapGain = {"0,10000", "10000,100000"};
	public String[] strCapLoss = {"0,2000", "2000,4356"};
	public String[] strhpw = {"1,47", "47,90"};
	public ArrayList<String[]> feature_list= new ArrayList<>();

	/**
	 * Adds List of Data to all the features
	 */
	RFData()
	{
		feature_list.add(strAge);
		feature_list.add(strWorkClass);
		feature_list.add(strfnlwgt);
		feature_list.add(strEducation);
		feature_list.add(strEduNum);
		feature_list.add(strMartial);
		feature_list.add(strOccupation);
		feature_list.add(strRelationship);
		feature_list.add(strRace);
		feature_list.add(strSex);
		feature_list.add(strCapGain);
		feature_list.add(strCapLoss);
		feature_list.add(strhpw);
		feature_list.add(strCountry);
	}

	/**
	 *
	 * @param req_list The List of data points
	 * @return Attribute ENtropy by Shannon's Formula
	 */
	public double attributeEntropy(ArrayList<String[]> req_list)
	{
		int g50 = 0, le50 = 0, size = req_list.size();
		for (String ad[] : req_list)
		{
			if (ad[14].equals("g50"))
			{
				g50++;
			}
			else if (ad[14].equals("le50"))
			{
				le50++;
			}
		}
		double p1 = (size != 0)? (g50/ (double)size): 0;
		double p2 = (size != 0)?(le50/ (double)size): 0;
		double t1_res = (p1!=0)?(-p1 * (Math.log(p1)/ Math.log(2))):0;
		double t2_res = (p2!=0)? p2 * (Math.log(p2)/Math.log(2)): 0;
		return t1_res - t2_res;
	}

	MaxGainStruct maxInformationGain(ArrayList<String []> req_list, int group[], boolean[] compare) throws NoSuchFieldException, IOException {
		int i, j, req_Index = -1;
		double res = 0, max_IG = 0.0;
		MaxGainStruct mgs = new MaxGainStruct();
		int group_size = group.length;
		mgs.position = -1; mgs.ig_value = 0.0;
		if (req_list.size() == 0) {return mgs;}
		for (i = 0; i < group_size; i++) {
			int feature = group[i];
			if (compare!= null && compare[i]){continue;}
			String[] list_values_in_feature = feature_list.get(feature);
			double ig = getInfoGain(req_list, feature, list_values_in_feature);
			if (ig > max_IG)
			{
				max_IG = ig;
				req_Index = i;
			}
		}
		mgs.ig_value = max_IG;
		mgs.position = req_Index;
		return mgs;
	}

	/**
	 *
	 * @param list List of Data
	 * @param feature_id Id of the feature the get Info Gain
	 * @param value_list The List of values in it
	 * @return The Information Gain IG(s, s(attr))
	 */
	private double getInfoGain(ArrayList<String []> list, int feature_id, String[] value_list)
	{
		int tc = list.size();
		double res = attributeEntropy(list);
		for (String value: value_list) {
			double count = 0;
			ArrayList<String []> new_list = new ArrayList<>();
			for (String[] each : list) {
				if (value.contains(","))
				{
					int small = Integer.parseInt(value.split(",")[0]);
					int large = Integer.parseInt(value.split(",")[1]);
					int val = Integer.parseInt(each[feature_id]);
					if (val >= small && val < large)
					{
						count++;
						new_list.add(each);
					}
				}
				else if (each[feature_id].equals(value)) {
					count++;
					new_list.add(each);
				}
			}
			res -= (count/ tc) * (attributeEntropy(new_list));
		}
		return res;
	}
}


class MaxGainStruct
{
	double ig_value;
	int position;
}

class FinalResult
{
	Id3TreeNode root;
	double accuracy;
	int[] list_of_features;
	FinalResult(Id3TreeNode root, double acc, int[] features)
	{
		this.root = root;
		this.accuracy = acc;
		this.list_of_features = features;
	}
}
