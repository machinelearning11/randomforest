package com.company;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException, NoSuchFieldException{
	// write your code here
		System.out.println();
		File input_file = new File("src/adult_mod.txt");
		File test_file = new File("src/test_mod.txt");
        RandomForest random_forest = new RandomForest(input_file, test_file);
        System.out.println("Random Tree Formation Method\n1. By random set of features");
        System.out.println("2. By random set of data\nChoose any one\n");
		Scanner sc = new Scanner(System.in);
		int choice = sc.nextInt();
		if (choice == 1 || choice == 2) {
			try {
				random_forest.startImplementation(choice);
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		else {
			System.out.println("Incorrect Option Selected. Exiting...");
		}
	}
}
